# Dementia Anti-Wandering (FYP 2021-2022)

## Initial Setup (Raspberry Pi)
### Headless Mode
> **IMPORTANT**: Deny all the system format warning during the setup.
1. Flash the micro SD card with [Raspberry Pi Imager](https://www.raspberrypi.org/software/).  
*Note: Available in choco with `choco install rpi-imager`.*
2. Download all the files in the `/setup/boot` folder from this repository.
3. Place all the downloaded files in the `Boot` drive of the micro SD card.
4. Modify the `wpa_supplicant.conf` file to fill in the required credentials for connecting Wi-Fi. Read more [here](https://www.raspberrypi.org/documentation/configuration/wireless/headless.md).  
*Note: `notepad.exe` should be sufficient, but preferrably use VSCode and set the line-ending to **LF** if not set already.*
5. SSH and Wi-Fi should be enabled upon booting. Use `ping raspberrypi.local` to determine the ip address of the device.  
*Note: Please patiently wait for up to 5 minutes for initial boot if ping does not respond.*
6. `ssh -o UserKnownHostsFile=NUL pi@<ip-address>` and enter the default password `raspberry` should get you a shell.  
*Note: Alternative command for ssh without ip address `ssh -o UserKnownHostsFile=NUL pi@raspberrypi` or `ssh -o UserKnownHostsFile=NUL pi@<hostname>`.*  
**IMPORTANT: The command for Linux or macOS is `ssh -o UserKnownHostsFile=/dev/null pi@<hostname/ip-address>`.**  
**IMPORTANT: If you encounter things like man-in-the-middle warning, run `clc ~/.ssh/known_hosts` on Windows PowerShell or `echo "" > ~/.ssh/known_hosts` on Linux or macOS terminal, then try again.**
7. Run the setup script by `curl -O https://kit-p.github.io/FYP_DAW/setup/setup.sh && source ./setup.sh`.  
**IMPORTANT: Make sure you have setup the ssh key in your GitHub account as specified in setup.sh**  
*Note: For security concern, you may inspect the script before executing.*

### Headfull Mode (i.e. Direct Control)
> **IMPORTANT**: Follow guide above for Headless Mode except skipping steps 2-6.

---

## Developing setup
> **IMPORTANT: Make sure you have run setup.sh before continuing in this section.**
### VSCode
1. Start VSCode on your own machine (not on the Pi).
2. Install [this remote SSH extension from Microsoft](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh).
3. Connect to the Pi (visit the above link for more detail).  
*Note: Before this point, you should have already changed the hostname and password of your Pi during setup.sh.*
4. Install the recommended extensions (i.e. [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) and optionally [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker))
5. During the setup.sh, the repository should have been cloned. Open the repository.
6. Press `F5` and VSCode will run the Python code in debugging mode.  
*Note: Press `Ctrl+Shift+B` will compile the Python code into a C executable and run it.*
7. Happy Developing!
