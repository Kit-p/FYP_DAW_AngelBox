"""
This type stub file was generated by pyright and manually typed.
"""

from typing import Any, Awaitable, Optional, Union

from .client import dictwrapper
from .gps import gps

"""aiogps.py -- Asyncio Python interface to GPSD.

This module adds asyncio support to the Python gps interface. It runs on
Python versions >= 3.6 and provides the following benefits:
    - easy integration in asyncio applications (all I/O operations done through
        non-blocking coroutines, async context manager, async iterator);
    - support for cancellation (all operations are cancellable);
    - support for timeouts (on both read and connect);
    - support for connection keep-alive (using the TCP keep alive mechanism)
    - support for automatic re-connection;
    - configurable connection parameters;
    - configurable exception handling (internally or by application);
    - logging support (logger name: 'gps.aiogps').

The use of timeouts, keepalive and automatic reconnection make possible easy
handling of GPSD connections over unreliable networks.

Examples:
    import logging
    import gps.aiogps

    # configuring logging
    logging.basicConfig()
    logging.root.setLevel(logging.INFO)
    # Example of setting up logging level for the aiogps logger
    logging.getLogger('gps.aiogps').setLevel(logging.ERROR)

    # using default parameters
    async with gps.aiogps.aiogps() as gpsd:
        async for msg in gpsd:
            # Log last message
            logging.info(f'Received: {msg}')
            # Log updated GPS status
            logging.info(f'\nGPS status:\n{gpsd}')

    # using custom parameters
    try:
        async with gps.aiogps.aiogps(
                connection_args = {
                    'host': '192.168.10.116',
                    'port': 2947
                },
                connection_timeout = 5,
                reconnect = 0,   # do not try to reconnect, raise exceptions
                alive_opts = {
                    'rx_timeout': 5
                }
            ) as gpsd:
            async for msg in gpsd:
                logging.info(msg)
    except asyncio.CancelledError:
        return
    except asyncio.IncompleteReadError:
        logging.info('Connection closed by server')
    except asyncio.TimeoutError:
        logging.error('Timeout waiting for gpsd to respond')
    except Exception as exc:
        logging.error(f'Error: {exc}')

"""
__all__ = ['aiogps']


class aiogps(gps):
    """An asyncio gps client.

    Reimplements all gps IO methods using asyncio coros. Adds connection
    management, an asyncio context manager and an asyncio iterator.

    The class uses a logger named 'gps.aiogps' to record events. The logger is
    configured with a NullHandler to disable any message logging until the
    application configures another handler.
"""

    def __init__(self, connection_args: Optional[dict[str, Any]] = ..., connection_timeout: Optional[float] = ..., reconnect: Optional[float] = ..., alive_opts: Optional[dict[str, Optional[int]]] = ...) -> None:
        """Arguments:
            connection_args: arguments needed for opening a connection.
                These will be passed directly to asyncio.open_connection.
                If set to None, a connection to the default gps host and port
                will be attempded.
            connection_timeout: time to wait for a connection to complete
                (seconds). Set to None to disable.
            reconnect: configures automatic reconnections:
                - 0: reconnection is not attempted in case of an error and the
                error is raised to the user;
                - number > 0: delay until next reconnection attempt (seconds).
            alive_opts: options related to detection of disconnections.
                Two mechanisms are supported: TCP keepalive (default, may not
                be available on all platforms) and Rx timeout, through the
                following options:
                - rx_timeout: Rx timeout (seconds). Set to None to disable.
                - SO_KEEPALIVE: socket keepalive and related parameters:
                - TCP_KEEPIDLE
                - TCP_KEEPINTVL
                - TCP_KEEPCNT
"""
        ...

    def __del__(self) -> None:
        """Destructor."""
        ...

    def close(self) -> None:
        """Closes connection to GPSD server."""
        ...

    def waiting(self) -> bool:
        """Mask the blocking waiting method from gpscommon."""
        ...

    async def read(self) -> Union[dictwrapper, str]:
        """Reads data from GPSD server."""
        ...

    async def connect(self) -> None:
        """Connects to GPSD server and starts streaming data."""
        ...

    async def send(self, commands: str | bytes) -> None:
        """Sends commands."""
        ...

    async def stream(self, flags: Optional[int] = ..., devpath: Optional[str] = ...) -> None:
        """Creates and sends the stream command."""
        ...

    async def __aenter__(self) -> aiogps:
        """Context manager entry."""
        ...

    async def __aexit__(self, exc_type: Any, exc: Any, traceback: Any) -> None:
        """Context manager exit: close connection."""
        ...

    def __aiter__(self) -> aiogps:
        """Async iterator interface."""
        ...

    async def __anext__(self) -> Union[dictwrapper, str]:
        """Returns next message from GPSD."""
        ...

    def __next__(self) -> Awaitable[Union[dictwrapper, str]]:
        """Reimplementation of the blocking iterator from gps.
        Returns an awaitable which returns the next message from GPSD.
"""
        ...
