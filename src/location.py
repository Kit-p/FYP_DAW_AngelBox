"""
This file contains the Location Module.
Location is the main class to be imported.
"""

import json
import logging
import sys
import threading
import time
from datetime import datetime, timezone
from time import sleep
from typing import Any, Final, Optional

import gps

from utils import JSON, print, restartSystemdService

# Global variable for Bluetooth Failure error code
GpsFailureCode: int = 102


class PositionEntry:
    """
    This class represents a location (GPS position) entry.
    """

    # timezone used for datetime related data, None means use local timezone
    _timezone: Final[Optional[timezone]] = None

    def __init__(self, mode: Optional[int] = None, time: Optional[str] = None, device: Optional[str] = None, latitude: Optional[float] = None, longitude: Optional[float] = None, altitude: Optional[float] = None, errLat: Optional[float] = None, errLon: Optional[float] = None, errAlt: Optional[float] = None, err2D: Optional[float] = None, err3D: Optional[float] = None):
        """
        This method is the constructor.
        It sets class variables with the parameters.
        It parses the GPS timestamp.
        It validates the entry by checking required attributes.
        """
        self.mode: Optional[int] = mode
        self.timestamp: Optional[int] = None
        if self.mode is not None and self.mode >= 2 and time is not None:
            try:
                # convert string timestamp to unix timestamp in milliseconds
                self.timestamp = int(datetime.strptime(
                    time, "%Y-%m-%dT%H:%M:%S.%fZ").replace(tzinfo=timezone.utc).timestamp() * 1e3)
            except:
                print("parse timestamp error")

        self.device: Optional[str] = device
        self.latitude: Optional[float] = latitude
        self.longitude: Optional[float] = longitude
        self.altitude: Optional[float] = altitude
        self.errLat: Optional[float] = errLat
        self.errLon: Optional[float] = errLon
        self.errAlt: Optional[float] = errAlt
        self.err2D: Optional[float] = err2D
        self.err3D: Optional[float] = err3D
        self.isValid = True

        if self.device is None or len(self.device) <= 0:
            self.isValid = False
        if self.mode is None or self.timestamp is None:
            self.isValid = False
        if self.latitude is None or self.longitude is None or self.err2D is None:
            self.isValid = False

    def __str__(self) -> str:
        """
        This method defines the string representation of the location entry.
        """
        jsonObject: JSON = JSON({
            "timestamp": self.timestamp,
            "device": self.device,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "altitude": self.altitude,
            "errLat": self.errLat,
            "errLon": self.errLon,
            "errAlt": self.errAlt,
            "err2D": self.err2D,
            "err3D": self.err3D,
        })

        return json.dumps(jsonObject)

    def print(self) -> None:
        """
        This method defines how the location entry should be printed for logging.
        """
        print(f"""
            PositionEntry {{
                isValid: {self.isValid}
                timestamp: {self.timestamp},
                device: {self.device},
                latitude: {self.latitude},
                longitude: {self.longitude},
                altitude: {self.altitude} m,
                errLat: {self.errLat} m,
                errLon: {self.errLon} m,
                errAlt: {self.errAlt} m,
                err2D: {self.err2D} m,
                err3D: {self.err3D} m,
            }}
        """, end="\n\n")


class Location:
    """
    This class provides interfaces for querying and updating location entry.
    """

    def __init__(self):
        """
        This method is the constructor.
        It creates an instance of the LocationThread.
        """
        self._thread: LocationThread = LocationThread(self)
        self._entry: PositionEntry = PositionEntry()
        self.isAlive = False

    def start(self):
        """
        This method starts the internal location thread.
        """
        try:
            self._thread.start()
        except:
            pass

    def stop(self):
        """
        This method stops the internal location thread.
        """
        self._thread.stop()

    def restart(self, renew: bool = False):
        """
        This method restarts, and optionally renews (re-creates), the internal location thread.
        """
        self.stop()
        if renew:
            self.__init__()
        self.start()

    def getLastValidPosition(self) -> Optional[PositionEntry]:
        """
        This method exposes the location entry if it is valid.
        """
        if self._entry is not None and self._entry.isValid:
            return self._entry
        return None

    def update(self, response: gps.dictwrapper) -> bool:
        """
        This method updates the location device or entry.
        It returns True if the update is successful.
        """
        resClass: str = getattr(response, "class", "")

        if resClass == "ERROR":
            # GPS service reports error
            logging.error(getattr(response, "message",
                          "An unknown error has occurred."))
            # renew the thread and hope the error disappears
            self.restart(renew=True)
        elif resClass == "DEVICES":
            # initial report of active devices
            devices: list[dict[str, Any]] = getattr(response, "devices", [])
            success: bool = True
            if len(devices) <= 0:
                logging.warning("No active GPS adapter")
            else:
                # update all location devices
                for device in devices:
                    if not self._updateDevice(gps.dictwrapper(device)):
                        success = False
            return success
        elif resClass == "DEVICE":
            # device activation / deactivation report
            return self._updateDevice(response)
        elif resClass == "TPV":
            # GPS position report
            return self._updatePosition(response)

        return False

    def _updateDevice(self, response: gps.dictwrapper) -> bool:
        """
        This method updates the location device.
        It returns True if the update is successful.
        """
        devicePath: str = getattr(response, "path", "An unknown device")
        activated: bool = getattr(response, "activated", False) != False
        activatedStr: str = "is detected but not activated."
        if activated:
            driver: str = getattr(response, "driver", "an unknown")
            activatedStr = f"is activated with {driver} driver."
        else:
            # device deactivated, Bluetooth Failure error
            self.isAlive = False
        print(f"{devicePath} {activatedStr}")
        return True

    def _updatePosition(self, response: gps.dictwrapper) -> bool:
        """
        This method updates the location entry.
        It returns True if the entry is valid and the update is successful.
        It parses the TPV message (example below).

        {
            'class': 'TPV',
            'device': '/dev/ttyACM0',
            'mode': 3,
            'time': '2022-02-22T10:56:16.000Z',
            'leapseconds': 18,
            'ept': 0.005,
            'lat': 22.390930833,
            'lon': 113.9745105,
            'altHAE': 50.9,
            'altMSL': 53.3,
            'alt': 53.3,
            'epx': 102.506,
            'epy': 44.542,
            'epv': 401.81,
            'track': 44.33,
            'magtrack': 41.3517,
            'magvar': -3.0,
            'speed': 1.407,
            'climb': 0.4,
            'eps': 205.01,
            'epc': 424.81,
            'geoidSep': -2.4,
            'eph': 154.47,
            'sep': 366.13
        }
        """
        entry = PositionEntry(
            mode=getattr(response, "mode", None),
            time=getattr(response, "time", None),
            device=getattr(response, "device", None),
            latitude=getattr(response, "lat", None),
            longitude=getattr(response, "lon", None),
            altitude=getattr(response, "altHAE", None),
            errLat=getattr(response, "epy", None),
            errLon=getattr(response, "epx", None),
            errAlt=getattr(response, "epv", None),
            err2D=getattr(response, "eph", None),
            err3D=getattr(response, "sep", None)
        )

        if entry.device is not None:
            # remove Bluetooth Failure error
            self.isAlive = True

        if entry.isValid:
            self._entry = entry

        return entry.isValid


class LocationThread(threading.Thread):
    """
    This class provides a separate thread for GPS listening.
    """

    def __init__(self, parent: Location):
        """
        This method is the constructor.
        It sets class variables with the parameters.
        """
        super().__init__(name="Thread-Location_gps", daemon=True)
        self.parent: Location = parent
        self.gps: Optional[gps.gps] = None
        self._shouldStop = True

    def run(self) -> None:
        """
        This method is automatically called when thread.start() is called.
        It is where listening operations take place.
        """
        self._shouldStop = False
        while self.gps is None and not self._shouldStop:
            try:
                # initialize the gpsd client
                self.gps = gps.gps(
                    mode=gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)
                print("gpsd started")
            except:
                # restart the systemd service and hope it functions properly
                restartSystemdService("gpsd.service")
                if not self._shouldStop:
                    # prevent the system from crashing
                    time.sleep(5)

        if self.gps is not None:
            while not self._shouldStop:
                try:
                    # synchronously query the next message from the gpsd client
                    response: gps.dictwrapper = next(self.gps)
                    # update the device / entry or report error
                    self.parent.update(response)
                except Exception as e:
                    logging.warning(str(e))

                    # restart the systemd service and hope the error goes away
                    restartSystemdService("gpsd.service")

                    if not self._shouldStop:
                        # prevent the system from crashing
                        time.sleep(5)
                        # re-initialize the gpsd client
                        self.gps = gps.gps(
                            mode=gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

        # cleanup
        self.stop()

    def stop(self) -> None:
        """
        This method sets the _shouldStop to cause the start() method to end.
        """
        self._shouldStop = True


def main():
    """
    This method is entrypoint of the Location Module.
    It is used to test the retrieve location data function.
    """
    # log level INFO or above
    logging.basicConfig(level=logging.INFO)
    location = Location()

    try:
        location.start()
        while True:
            ent = location.getLastValidPosition()
            if ent:
                ent.print()
            else:
                print("No valid position yet")
            sleep(3)
    except KeyboardInterrupt:
        location.stop()
        sys.exit(0)


if __name__ == "__main__":
    main()
