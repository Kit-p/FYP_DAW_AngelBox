"""
This file contains the Detection Module.
BLEScanner is the main class to be imported.
"""

from __future__ import annotations

import json
import logging
import threading
import time
from datetime import datetime, timezone
from string import Template
from typing import Final, Optional
from uuid import UUID

import paho.mqtt.client as mqtt
from bluepy.btle import DefaultDelegate, ScanEntry, Scanner

from __init__ import __version__
from location import GpsFailureCode, Location, PositionEntry
from networking import Networking
from utils import (JSON, fixPermissions, getConfig, getUnixMilliTimestamp,
                   print, restartSystemdService)

# Global variable for Bluetooth Failure error code
BluetoothFailureCode: int = 101


class BeaconEntry:
    """
    This class represents a scanned BLE beacon (in iBeacon format) entry.
    """

    # timezone used for datetime related data, None means use local timezone
    _timezone: Final[Optional[timezone]] = None
    # string template for creating a unique identifier of the BLE beacon
    _idTemplate: Final[Template] = Template("${uuid}_${major}_${minor}")

    def __init__(self, scanEntry: ScanEntry, angelboxId: str, startupTimestamp: int, locationHandler: Location, version: Optional[str] = None):
        """
        This method is the constructor.
        It sets class variables with the parameters.
        It parses the iBeacon packet in scanEntry and fills in the computed properties.
        It validates the beacon entry with UUID filtering.
        It retrieves location information from the Location Module and fills in the computed properties.
        It reports GPS Failure error if location information cannot be retrieved.
        """
        self.angelboxId = angelboxId
        self.startupTimestamp = startupTimestamp
        self.version = version
        self.timestamp: Final[int] = getUnixMilliTimestamp(self._timezone)
        self.macAddr: Optional[str] = scanEntry.addr
        self.rssi: Optional[int] = scanEntry.rssi

        uuid: Optional[UUID] = None
        major: Optional[int] = None
        minor: Optional[int] = None
        txPower: Optional[int] = None
        isValid: Optional[bool] = False

        # get raw packet of custom manufacturer data
        data: bytes = scanEntry.getValue(0xFF)
        # check if packet is in iBeacon format
        if (type(data) is bytes and len(data) == 25
                and data[:4] == bytes.fromhex("4C000215")):
            # reference Wikipedia or AprilBrother Wiki for iBeacon format parsing
            uuid = UUID(bytes=data[4:20])
            major = int.from_bytes(data[20:22], byteorder="big", signed=False)
            minor = int.from_bytes(data[22:24], byteorder="big", signed=False)
            txPower = data[24]

            # filter beacon by UUID
            config: Optional[JSON] = getConfig()
            if config is not None:
                uuids = config.get("uuids", None)
                if uuids is None:
                    isValid = True
                elif type(uuids) is list:
                    # uuids is a list of string, match one by one
                    uuids: list[str]
                    for target in uuids:
                        if uuid == UUID(target):
                            isValid = True
                            break
                elif type(uuids) is str:
                    # uuids is a string, match directly
                    if uuid == UUID(uuids):
                        isValid = True
            else:
                isValid = True

        self.uuid: Final[Optional[UUID]] = uuid
        self.major: Final[Optional[int]] = major
        self.minor: Final[Optional[int]] = minor
        self.txPower: Final[Optional[int]] = txPower
        self.isValid: Final[Optional[bool]] = isValid
        self.locationX: Optional[float] = None
        self.locationY: Optional[float] = None
        self.locationZ: Optional[float] = None
        self.err2D: Optional[float] = None
        self.gpsTimestamp: Optional[int] = None

        # retrieve latest position information from the Location Module
        positionEntry: Optional[PositionEntry] = locationHandler.getLastValidPosition(
        )
        if positionEntry is not None:
            self.locationX = positionEntry.longitude
            self.locationY = positionEntry.latitude
            self.locationZ = positionEntry.altitude
            self.err2D = positionEntry.err2D
            self.gpsTimestamp = positionEntry.timestamp

        self.errorCodes: list[int] = []
        if not locationHandler.isAlive:
            # report GPS Failure error
            self.errorCodes.append(GpsFailureCode)

    def __str__(self) -> str:
        """
        This method defines the string representation of the beacon entry.
        """
        jsonObject: JSON = JSON({
            "version": self.version,
            "angelboxId": self.angelboxId,
            "timestamp": self.timestamp,
            "macAddr": self.macAddr,
            "uuid": str(self.uuid),
            "major": self.major,
            "minor": self.minor,
            "rssi": self.rssi,
            "txPower": self.txPower,
            "locationX": self.locationX,
            "locationY": self.locationY,
            "locationZ": self.locationZ,
            "err2D": self.err2D,
            "gpsTimestamp": self.gpsTimestamp,
            "errorCodes": self.errorCodes,
            "startupTimestamp": self.startupTimestamp,
        })

        return json.dumps(jsonObject)

    def print(self) -> None:
        """
        This method defines how the beacon entry should be printed for logging.
        """
        timestampString: str = \
            datetime.fromtimestamp(self.timestamp/1e3, tz=self._timezone)\
                    .strftime("%Y/%m/%d %H:%M:%S.%f (%Z)")
        print(f"""
            BeaconEntry {{
                isValid: {self.isValid}
                angelboxId: {self.angelboxId}
                timestamp: {self.timestamp}\t'{timestampString}'
                macAddr: {self.macAddr}
                uuid: {self.uuid}
                major: {self.major}
                minor: {self.minor}
                rssi: {self.rssi}
                txPower: {self.txPower}
                locationX: {self.locationX}
                locationY: {self.locationY}
                locationZ: {self.locationZ}
                err2D: {self.err2D}
                gpsTimestamp: {self.gpsTimestamp}
                errorCodes: {self.errorCodes},
                startupTimestamp: {self.startupTimestamp}
            }}
        """, end="\n\n")


class BLEScanner:
    """
    This class provides a BLE scanner.
    """

    def __init__(self, angelboxId: str, startupTimestamp: int, locationHandler: Location, networkHandler: Networking):
        """
        This method is the constructor.
        It first fixes the execution permission issue for the bluepy-helper program.
        It creates an instance of Scanner with the custom ScanDelegate internal.
        It sets class variables with the parameters.
        """
        fixPermissions()
        self._scanner: Scanner = Scanner().withDelegate(
            ScanDelegate(self, angelboxId, startupTimestamp, locationHandler))
        self._scanThread: ScanThread = ScanThread(self, self._scanner)
        self._networkHandler: Networking = networkHandler
        self.version = self._networkHandler.version
        self._buffer: list[BeaconEntry] = []
        self.isAlive: bool = False

    def clear(self, numElement: Optional[int] = None) -> None:
        """
        This method clears the internal beacon entry buffer or removes the first numElement entries.
        """
        if numElement is None:
            self._buffer.clear()
        else:
            del self._buffer[:numElement]

    def start(self) -> None:
        """
        This method starts the internal scan thread.
        """
        try:
            self._scanThread.start()
        except:
            pass

    def stop(self, timeout: Optional[float] = None) -> bool:
        """
        This method stops and waits for the internal scan thread to terminate.
        It returns True if successfully stopped scanning.
        """
        self._scanThread.stop()
        self._scanThread.join(timeout)
        return (not self._scanThread.is_alive())

    def addEntry(self, entry: BeaconEntry) -> bool:
        """
        This method adds a new beacon scan entry into the internal buffer.
        It returns True if successfully added/updated the entry.
        """
        if not entry.isValid:
            return False

        # # * Note: for determining advertisement packet loss rate
        # if entry.macAddr != "xx:xx:xx:xx:xx:xx":  # e.g., "f7:ac:f5:c5:30:09"
        #     return False

        self._buffer.append(entry)
        return True

    def publishEntry(self, entry: BeaconEntry) -> bool:
        """
        This method publishes a beacon scan entry with the Network Module.
        It returns True if successfully send the entry.
        """
        if not entry.isValid:
            return False

        # publish the entry
        result = self._networkHandler.publish(
            Networking.SCAN_TOPIC, str(entry), 1, False)

        if result.rc != mqtt.MQTT_ERR_SUCCESS:
            print("Publish scan result: fail")
            return False

        print("Publish scan result: success")
        return True

    def getEntries(self) -> list[BeaconEntry]:
        """
        This method exposes the internal buffer.
        """
        return self._buffer


class ScanThread(threading.Thread):
    """
    This class provides a separate thread for beacon scanning.
    """

    def __init__(self, bleScanner: BLEScanner, scanner: Scanner):
        """
        This method is the constructor.
        It sets class variables with the parameters.
        """
        super().__init__(name="Thread-BLEScanner_scan", daemon=True)
        self.scanner: Scanner = scanner
        self.bleScanner: BLEScanner = bleScanner
        self._shouldStop: bool = True

    def run(self) -> None:
        """
        This method is automatically called when thread.start() is called.
        It is where scanning operations take place.
        """
        self._shouldStop = False
        while not self._shouldStop:
            try:
                self.bleScanner.isAlive = True
                # setting timeout to > 0, such that error of hardware or systemd service throws an Exception
                # passive scan enables continuous scan (instead of having cooldown)
                self.scanner.scan(1, passive=True)
            except Exception as e:
                self.bleScanner.isAlive = False
                if not self._shouldStop:
                    logging.error(str(e))
                    # attempt to rectify the problem by restarting the systemd service
                    restartSystemdService("bluetooth.service")
                    # prevent crashing the system
                    time.sleep(5)

        self.bleScanner.isAlive = False
        # cleanup
        self.stop()

    def stop(self) -> None:
        """
        This method stops the internal scanner.
        """
        self._shouldStop = True
        try:
            self.scanner.stop()
        except:
            pass


class ScanDelegate(DefaultDelegate):
    """
    This class is the underlying logic for the beacon scanner.
    """

    def __init__(self, bleScanner: BLEScanner, angelboxId: str, startupTimestamp: int, locationHandler: Location):
        """
        This method is the constructor.
        It sets class variables with the parameters.
        """
        super().__init__()
        self._scanner: BLEScanner = bleScanner
        self._angelboxId = angelboxId
        self._locationHandler: Location = locationHandler
        self._startupTimestamp: int = startupTimestamp

    def handleDiscovery(
        self,
        scanEntry: ScanEntry,
        isNewDev: bool,
        isNewData: bool
    ) -> None:
        """
        This method is the handler for the device discovered event.
        It attempts the create and publish a beacon scan entry.
        """
        # validation done in constructor
        entry = BeaconEntry(scanEntry, self._angelboxId,
                            self._startupTimestamp, self._locationHandler, self._scanner.version)
        print(f"""
            isNewDev: {isNewDev}
            isNewData: {isNewData}
        """)
        entry.print()
        # fail if beacon scan entry is invalid
        self._scanner.publishEntry(entry)


def main():
    """
    This method is entrypoint of the Detection Module.
    It is used to test the beacon scan functionality.
    """
    # log level INFO or above
    logging.basicConfig(level=logging.INFO)
    startupTimestamp: Final[int] = getUnixMilliTimestamp()
    locationHandler: Location = Location()
    networkHandler: Networking = Networking(
        "test", "mqtt-angelbox.fyp2122-gch2-daw.tk", 443, __version__)
    bleScanner: BLEScanner = BLEScanner(
        "test", startupTimestamp, locationHandler, networkHandler)

    try:
        bleScanner.start()
        while True:
            pass
    except KeyboardInterrupt:
        pass
    finally:
        bleScanner.stop()
        logging.shutdown()


if __name__ == "__main__":
    main()
