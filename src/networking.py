"""
This file contains the Network Module.
Networking is the main class to be imported.
"""

import json
import logging
import os
import ssl
import time
from typing import Any, Callable, Optional

import paho.mqtt.client as mqtt

from __init__ import __version__
from management import Command, handleCommand, handlePostCommandResponse
from utils import (JSON, getConfig, getUnixMilliTimestamp, init, print,
                   registerAsSystemdService, setLastCmd)


class Networking:
    """
    This class provides interfaces for publishing with the internal MQTT client.
    It also handles subscribing for command topics and calling functions in the Management Module.
    """

    # use the local version of the CA cert so both environment works
    # alternatively, import the system path from utils.py
    MQTT_CA_CERT_PATH = os.path.join(
        os.path.dirname(__file__), "etc/MQTT_CA.crt")
    # root segment of the MQTT topics
    NAMESPACE = "DAW"
    # topic constants
    TEST_TOPIC = f"{NAMESPACE}/test"
    SCAN_TOPIC = f"{NAMESPACE}/scan"
    STATUS_TOPIC = f"{NAMESPACE}/status"
    COMMAND_TOPIC = f"{NAMESPACE}/command"

    def __init__(self, id: str, broker_addr: str, broker_port: int, version: str):
        """
        This method is the constructor.
        It sets class variables with the parameters.
        It sets the default on connect event and on message event handlers.
        It attempts to connect to the MQTT broker in the backend system.
        Note: The "COMMAND_TOPIC/{angelboxId}" topic is auto-subscribed, please refer to proxy subscription in EMQX documentation.
        """
        self.client_id = id
        self.version = version
        self.user_data = None
        self.broker_addr = broker_addr
        self.broker_port = broker_port
        self.keepalive_period = 60   # seconds
        # create an instance of the MQTT client
        self._client = mqtt.Client(
            self.client_id, self.client_id == "", self.user_data)
        # dummy password, authentication by client ID
        self._client.username_pw_set("angelbox", "angelbox")
        # authenticate with the MQTT broker using one-way SSL
        self._client.tls_set(
            ca_certs=self.MQTT_CA_CERT_PATH, tls_version=ssl.PROTOCOL_TLS)

        # set the default handlers
        self._client.on_connect = self._on_connect
        self._client.on_message = self._on_message

        # attempt to connect ot he MQTT broker
        print(f"Connecting to {self.broker_addr}:{self.broker_port}")
        while True:
            try:
                self._client.connect(
                    self.broker_addr, self.broker_port, self.keepalive_period)
                break
            except:
                print("Fail initial mqtt connection")
                # retry after 5 seconds to prevent system from crashing
                time.sleep(5)

        # subscribe to the test topic for subscription function testing
        result = self._client.subscribe(self.TEST_TOPIC, 1)
        if result[0] == mqtt.MQTT_ERR_SUCCESS:
            print(f"Subscribed to {self.TEST_TOPIC}")
        else:
            print(f"Failed when attempting to subscribe {self.TEST_TOPIC}")

    def start(self):
        """
        This method starts the MQTT client so it listens to incoming messages.
        """
        self._client.loop_start()

    def stop(self):
        """
        This method stops the MQTT client.
        """
        self._client.loop_stop()

    @property
    def publish(self):
        """
        This property exposes the "publish" method of the internal MQTT client for external use.
        It is needed to preserve intellisense in IDE.
        """
        return self._client.publish

    def _publishCommandResponse(self, id: str, success: bool, response: Optional[str], timestamp: Optional[int] = None, callback: Optional[Callable[[], Any]] = None) -> bool:
        """
        This method publishes a command response to the COMMAND_TOPIC.
        It executes the specific callback (if any) after the a successful publish.
        It returns True if both the publishing and the execution of the callback are successful.
        """
        # default message timestamp to current time
        if timestamp is None:
            timestamp = getUnixMilliTimestamp()

        # build the message
        msg: JSON = JSON({
            "version": self.version,
            "command_id": id,
            "angelbox_id": self.client_id,
            "is_success": success,
            "response": response,
            "timestamp": timestamp
        })
        publishMessage: str = json.dumps(msg)

        # publish the message
        result = self.publish(self.COMMAND_TOPIC, publishMessage, 1, False)
        if result.rc == mqtt.MQTT_ERR_SUCCESS:
            print("Publish command response: success")
        elif result.rc == mqtt.MQTT_ERR_NO_CONN:
            logging.error("Publish command response: fail no connection")
            return False
        elif result.rc == mqtt.MQTT_ERR_QUEUE_SIZE:
            logging.error("Publish command response: fail max queue")
            return False

        if callback is not None:
            try:
                # synchronously wait for the message to be published
                result.wait_for_publish(10)
                # execute the callback
                callback()
            except Exception as e:
                logging.error(str(e))
                return False

        return True

    def _on_connect(self, client: mqtt.Client, userdata: Any, flags: dict[str, int], rc: int):
        """
        This method is the handler for the on connect event.
        It publishes command response if any.
        It restarts the driver program if the publishing has failed.
        """
        print(mqtt.connack_string(rc))
        if rc != 0:
            return
        session_present = flags["session present"]
        print(
            f"Is Reusing Previous Session: {session_present} ({session_present == 1})")

        # generate the command response
        id, success, response = handlePostCommandResponse()
        if id is None or id == "":
            return

        # publish the command response
        success = self._publishCommandResponse(
            id, success, response, callback=lambda: setLastCmd(None))

        if not success:
            # re-register as systemd service and restarts the service and hope the problem goes away
            registerAsSystemdService(True)

    def _on_message(self, client: mqtt.Client, userdata: Any, message: mqtt.MQTTMessage):
        """
        This method is the handler for the on message event.
        It processes messages from subscribed topics and take actions correspondingly.
        It now handles command messages and calls functions in the Management Module to execute the specific command.
        """
        print(f"Message Received from {message.topic}:")
        print(str(message.payload))
        if message.topic.startswith(Networking.COMMAND_TOPIC):
            # handle command message
            id, success, response = handleCommand(message.payload)
            if id is None or id == "":
                return
            # publish command response immediately
            # for destructive commands (reboot / update), this line will not be executed, so need to rely on the _on_connect() handler to publish the response
            self._publishCommandResponse(
                id, success, response, callback=lambda: setLastCmd(None))


def main():
    """
    This method is entrypoint of the Networking Module.
    It is used to test the connectivity with the MQTT broker and basic functions like publish and subscribe.
    """
    # log level INFO or above
    logging.basicConfig(level=logging.INFO)

    # initialize the environment
    init(Command.UPDATE)

    # read the config
    config: Optional[JSON] = getConfig()
    if config is None:
        logging.critical("Failed to start networking module: missing config")
        return
    id: Optional[str] = config.get("id")
    if id is None or type(id) is not str or len(id) <= 0:
        logging.critical("Missing AngelBox ID")
        return
    mqttConfig: Optional[JSON] = config.get("mqtt")
    if mqttConfig is None:
        logging.critical("Missing MQTT config")
        return
    broker_addr: Optional[str] = mqttConfig.get("broker_address")
    if broker_addr is None or type(broker_addr) is not str:
        logging.critical("Missing MQTT broker address")
        return
    broker_port: Optional[int] = mqttConfig.get("broker_port")
    if broker_port is None or type(broker_port) is not int:
        logging.critical("Missing MQTT broker port")
        return
    networking = Networking(id, broker_addr, broker_port, __version__)

    try:
        networking.start()
        print(f"Publishing a message to {Networking.TEST_TOPIC}...")
        networking.publish(Networking.TEST_TOPIC,
                           "angelbox test", 1, False)
        while True:
            pass
    except KeyboardInterrupt:
        pass
    finally:
        networking.stop()
        logging.shutdown()


if __name__ == "__main__":
    main()
