"""
This file contains utility functions for handling low level interaction with the system.
"""

from __future__ import annotations

import json
import logging
import os
import re
import shutil
import subprocess
import sys
import time
import uuid
from builtins import print as _print
from datetime import datetime, timezone
from inspect import cleandoc
from os import path
from typing import Any, Final, NewType, Optional

import requests

from __init__ import __version__

# global variable for a custom JSON type definition
JSON = NewType("JSON", dict[str, Any])

# global variables for local paths (inside repository)
DEFAULT_DIR: Final[str] = os.path.dirname(__file__)
DEFAULT_ETC_DIR: Final[str] = os.path.join(DEFAULT_DIR, "etc")
DEFAULT_CONFIG_PATH: Final[str] = os.path.join(DEFAULT_ETC_DIR, "config.json")
DEFAULT_MQTT_CACERT_PATH: Final[str] = os.path.join(
    DEFAULT_ETC_DIR, "MQTT_CA.crt")
DEFAULT_SYSTEMD_UNITFILE_PATH: Final[str] = os.path.join(
    DEFAULT_ETC_DIR, "angelbox.service")

# global variables for system paths (outside repository)
SYSTEM_DIR: Final[str] = "/opt/angelbox"
SYSTEM_BIN_DIR: Final[str] = os.path.join(SYSTEM_DIR, "bin")
SYSTEM_BINARY_PATH: Final[str] = os.path.join(SYSTEM_BIN_DIR, "angelbox.bin")
SYSTEM_ETC_DIR: Final[str] = os.path.join(SYSTEM_DIR, "etc")
CONFIG_PATH: Final[str] = os.path.join(SYSTEM_ETC_DIR, "config.json")
MQTT_CACERT_PATH: Final[str] = os.path.join(SYSTEM_ETC_DIR, "MQTT_CA.crt")
SYSTEMD_SERVICE_NAME: Final[str] = "angelbox.service"
SYSTEMD_UNITFILE_PATH: Final[str] = os.path.join(
    SYSTEM_ETC_DIR, SYSTEMD_SERVICE_NAME)
ID_PATH: Final[str] = os.path.join(SYSTEM_DIR, "device_id")
LAST_CMD_PATH: Final[str] = os.path.join(SYSTEM_DIR, "last_cmd")


def init(updateCommandCode: Optional[int] = None):
    """
    This function initializes the system environment by copying all the necessary files from local paths to system paths.
    It also registers the driver program as a systemd service if needed.
    It restarts the current instance of the driver program when needed (after an update).
    """
    logging.info(f"Initialzing AngelBox Driver Program v{__version__}")

    # read the last executed command
    lastCmd = getLastCmd()
    cleanAndRestart = False
    register = False
    if lastCmd is not None and lastCmd.get("command", -1) == updateCommandCode:
        if lastCmd.get("hasRestarted", False) == False:
            # have not restarted after an update command, restart to cleanly reload all configurations
            cleanAndRestart = True
            lastCmd["hasRestarted"] = True
            setLastCmd(lastCmd)

    if cleanAndRestart:
        # remove all old configurations
        shutil.rmtree(SYSTEM_ETC_DIR, True)

    # create necessary directories in the system and copy over files from local paths
    if not os.path.isdir(SYSTEM_BIN_DIR):
        register = True
        os.makedirs(SYSTEM_BIN_DIR, 0o755, True)
    if not os.path.isfile(SYSTEM_BINARY_PATH):
        register = True
        shutil.copyfile(os.path.abspath(sys.argv[0]),
                        SYSTEM_BINARY_PATH, follow_symlinks=True)
        os.chmod(SYSTEM_BINARY_PATH, 0o777)
    if not os.path.isdir(SYSTEM_ETC_DIR):
        register = True
        os.makedirs(SYSTEM_ETC_DIR, 0o755, True)
    if not os.path.isfile(CONFIG_PATH):
        register = True
        shutil.copyfile(DEFAULT_CONFIG_PATH, CONFIG_PATH, follow_symlinks=True)
    if not os.path.isfile(MQTT_CACERT_PATH):
        register = True
        shutil.copyfile(DEFAULT_MQTT_CACERT_PATH,
                        MQTT_CACERT_PATH, follow_symlinks=True)
    if not os.path.isfile(SYSTEMD_UNITFILE_PATH):
        register = True
        shutil.copyfile(DEFAULT_SYSTEMD_UNITFILE_PATH,
                        SYSTEMD_UNITFILE_PATH, follow_symlinks=True)
    if not os.path.isfile(LAST_CMD_PATH):
        register = True
        # create an empty file
        open(LAST_CMD_PATH, "w").close()

    if cleanAndRestart or register:
        # register and restart as systemd service
        registerAsSystemdService(True)


def fixPermissions():
    """
    This function is used to fix file permissions of library data files due to problems within the Nuitka compilation.
    """
    # fix bluepy/bluepy-helper missing executable permission
    bluepyDir: Final[str] = os.path.join(DEFAULT_DIR, "bluepy")
    if os.path.isdir(bluepyDir):
        bluepyHelperPath: Final[str] = os.path.join(bluepyDir, "bluepy-helper")
        os.chmod(bluepyHelperPath, 0o555)


def registerAsSystemdService(restart: Optional[bool] = False):
    """
    This function registers the driver program as a systemd service and restart as the service if specified.
    """
    # disable and remove old systemd service definitions
    subprocess.run(["systemctl", "disable", SYSTEMD_SERVICE_NAME])
    subprocess.run(["systemctl", "link", SYSTEMD_UNITFILE_PATH])
    # reload systemd to read in new definition files
    subprocess.run(["systemctl", "daemon-reload"])
    # enable the systemd service
    subprocess.run(["systemctl", "enable", SYSTEMD_SERVICE_NAME])
    logging.info("Registered as systemd service")

    if restart:
        # restart the current instance of the driver program as a systemd service
        logging.info("Restarting program as systemd service...")
        restartSystemdService(SYSTEMD_SERVICE_NAME)
        sys.exit(0)  # gurrantee termination


def restartSystemdService(name: str):
    """
    This function restarts a given systemd service.
    """
    print(f"Restarting systemd service \"{name}\"")
    subprocess.run(["systemctl", "restart", name])


def getConfig(path: str = CONFIG_PATH) -> Optional[JSON]:
    """
    This function reads the configuration file and converts it to a JSON object.
    """
    config = readJsonFile(path)
    if config is None:
        logging.warn(f"Failed to load config file at '{path}'!")
    return config


def getHardwareMacAddr() -> str:
    """
    This function gets the hardware MAC address of the system.
    """
    macAddr, iface = getHardwareMacAddrFromSysFile()

    if macAddr is None:
        # backup solution to get a MAC address (highly likely to be a random one)
        rawMacAddr: int = uuid.getnode()
        iface = "RANDOM"
        macAddr = ":".join(re.findall("..", "%012x" % rawMacAddr))

    logging.info(f"Hardware MAC address is {macAddr} of interface [{iface}]")
    return macAddr


def getHardwareMacAddrFromSysFile() -> tuple[Optional[str], Optional[str]]:
    """
    This function reads the system pseudo-files to find the hardware MAC address.
    It returns a tuple of (macAddress, interface) if success, otherwise (None, None).
    """
    # list of network interfaces to search
    ifaces: Final[list[str]] = ["eth", "wlan", "eno", "ens", "enp2s"]
    macAddr: Optional[str] = None
    iface: Optional[str] = None

    for _iface in ifaces:
        for i in range(0, 10):
            # iface is something like eth0, wlan1, eno2, ens3, enp2s4, etc.
            iface = _iface + str(i)
            macAddrPath = f"/sys/class/net/{iface}/address"
            if not os.path.isfile(macAddrPath):
                continue

            try:
                # read the content of the pseudo-file
                with open(macAddrPath, mode="r") as macAddrFile:
                    macAddr = macAddrFile.read().strip()  # strip the trailing newline
                    if len(macAddr) == 17:  # 6 bytes (each 2 digits) + 5 colons
                        return (macAddr, iface)
            except:
                pass

    # cannot read hardware MAC address from the pseudo-files
    logging.warn(
        "Failed to retrieve hardware MAC address from /sys/class/net/* paths!")
    return (None, None)


def checkAngelboxIdFileExist(filePath: str = ID_PATH) -> bool:
    """
    This function checks if the device_id file exists.
    """
    return path.isfile(filePath)


def getAngelboxId(path: str = ID_PATH) -> Optional[str]:
    """
    This function reads the content of the device_id file.
    It returns the device ID if successful otherwise None.
    """
    try:
        with open(path, mode="r") as idFile:
            return idFile.read()
    except:
        logging.warn(f"Failed to load id file at '{path}'!")
        return None


def registerAngelbox(api_endpoint: str, mac_address: Optional[str], version: Optional[str]) -> Optional[str]:
    """
    This function registers the AngelBox as a pending box in the backend system.
    It retrieves a unique ID from the backend system.
    """
    if mac_address is None or len(mac_address) <= 0:
        logging.warn("Missing Hardware Mac address")
        return
    if version is None or len(version) <= 0:
        logging.warn("Missing Version")
        return
    while True:
        try:
            # send a HTTP POST request to the API server
            r = requests.post(
                api_endpoint,
                json={
                    'mac_address': mac_address,
                    'version': version,
                },
                timeout=15
            )
            # raise exception if status >= 4XX
            r.raise_for_status()
            # convert the response body to a JSON object
            r_json: JSON = r.json()
            # read the ID generated by the backend system
            new_id: Optional[str] = r_json.get("id")
            # close the connection for cleanup
            r.close()
            return new_id
        except Exception as e:
            logging.warn(
                f"Fail registering angelbox! Error: {e}")
            # retry after 10 seconds to prevent the system from crashing
            time.sleep(10)


def setAngelboxId(id: str, path: str = ID_PATH):
    """
    This function writes the device ID to a file in the system path.
    """
    try:
        with open(path, mode="w") as idFile:
            idFile.write(id)
    except:
        logging.warn(f"Failed to write id file at '{path}'!")


def getLastCmd(path: str = LAST_CMD_PATH) -> Optional[JSON]:
    """
    This function reads the last executed command and converts it to a JSON object.
    """
    last_cmd = readJsonFile(path)
    return last_cmd


def setLastCmd(payload: Optional[JSON], path: str = LAST_CMD_PATH):
    """
    This function writes the last executed to a file in the system path.
    It clears the content in the file if the payload is not supplied.
    """
    if payload is None:
        # clear the content
        open(path, mode="w").close()
    elif not writeJsonFile(path, payload):
        logging.warn(f"Failed to write last cmd file at '{path}'!")


def readJsonFile(path: str) -> Optional[JSON]:
    """
    This function reads a JSON file in the system and converts it to a JSON object.
    """
    try:
        with open(path, mode="r", encoding="utf8") as file:
            return JSON(json.load(file))
    except:
        return None


def writeJsonFile(path: str, payload: JSON) -> bool:
    """
    This function dumps the JSON object to a JSON string into a file in the system (overwriting).
    """
    try:
        with open(path, mode="w", encoding="utf8") as file:
            json.dump(payload, file, skipkeys=False, ensure_ascii=False,
                      check_circular=True, allow_nan=True, indent="\t", sort_keys=False)
            return True
    except:
        return False


def getUnixMilliTimestamp(timezone: Optional[timezone] = None) -> int:
    """
    This function gets the current timestamp and converts it to a unix timestamp in milliseconds.
    It returns current timestamp in milliseconds with 13 digits.
    """
    return int(datetime.now(tz=timezone).timestamp() * 1e3)


def print(
    *values: object,
    sep: Optional[str] = None,
    end: Optional[str] = None,
    file: Any = None,
    flush: bool = False
) -> None:
    """
    This function wraps the built-in print function to make the print dependent on the debug attribute in the config file.
    """
    config: Optional[JSON] = getConfig()
    if config is None:
        return

    isDebug: bool = config.get("debug", False)
    if not isDebug:
        return

    if len(values) == 1 and type(values[0]) is str:
        _print(cleandoc(values[0]), sep=sep, end=end, file=file, flush=flush)
    else:
        _print(*values, sep=sep, end=end, file=file, flush=flush)
