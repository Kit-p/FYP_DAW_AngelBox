"""
This file contains the entrypoint of the driver program.
AngelBox is the main class to be imported.
"""

from __future__ import annotations

import json
import logging
import sys
import threading
import time
from typing import Final, Optional

import paho.mqtt.client as mqtt

from __init__ import __version__
from bluetooth import BLEScanner, BluetoothFailureCode
from location import GpsFailureCode, Location, PositionEntry
from management import Command
from networking import Networking
from utils import (JSON, checkAngelboxIdFileExist, getAngelboxId, getConfig,
                   getHardwareMacAddr, getUnixMilliTimestamp, init, print,
                   registerAngelbox, setAngelboxId)


class AngelBox:
    """
    This class wraps the AngelBox as an object.
    It provides interfaces to manage the AngelBox and its modules.
    """

    def __init__(self, id: str, broker_addr: str, broker_port: int):
        """
        This method is the constructor.
        It sets class variables with the parameters.
        It creates an instance of all the modules.
        """
        self.startupTimestamp: Final[int] = getUnixMilliTimestamp()
        self.version: Final[str] = __version__
        self.id: str = id
        self._shouldStop: bool = False
        self.locationHandler: Location = Location()
        self.networkHandler: Networking = Networking(
            id, broker_addr, broker_port, self.version)
        self.bleScanner: BLEScanner = BLEScanner(self.id, self.startupTimestamp,
                                                 self.locationHandler, self.networkHandler)
        self._heartbeatThread: HeartbeatThread = HeartbeatThread(
            self.id, self.bleScanner, self.locationHandler, self.networkHandler, self.startupTimestamp, self.version)

    def start(self) -> None:
        """
        This method starts the modules.
        """
        logging.info(
            f"Starting AngelBox with id {self.id} on version {self.version}")
        self._shouldStop = False
        self.locationHandler.start()
        # Management Module does not require a thread, will be called by the Network Module on demand.
        self.networkHandler.start()
        self.bleScanner.start()
        try:
            # start the heartbeat thread
            self._heartbeatThread.start()
        except:
            pass

        while not self._shouldStop:
            pass

        # cleanup
        self.stop()

    def stop(self) -> None:
        """
        This method stops all modules.
        """
        self._shouldStop = True
        self._heartbeatThread.stop()
        self.bleScanner.stop()
        self.networkHandler.stop()
        self.locationHandler.stop()


class HeartbeatThread(threading.Thread):
    """
    This class provides a separate thread for heartbeat publishing.
    """

    def __init__(self, id: str, bleScanner: BLEScanner, locationHandler: Location, networkHandler: Networking, startupTimestamp: int, version: str):
        """
        This method is the constructor.
        It sets class variables with the parameters.
        """
        super().__init__(name="Thread-Angelbox_heartbeat", daemon=True)
        self.id: str = id
        self.bleScanner: BLEScanner = bleScanner
        self.locationHandler: Location = locationHandler
        self.networkHandler: Networking = networkHandler
        self.startupTimestamp: Final[int] = startupTimestamp
        self.version: Final[str] = version
        self._shouldStop: bool = True

    def run(self) -> None:
        """
        This method is automatically called when thread.start() is called.
        It is where regular heartbeat publishing takes place.
        """
        self._shouldStop = False
        # buffer time for module initialization
        time.sleep(5)

        while not self._shouldStop:
            # get location
            locationX: Optional[float] = None
            locationY: Optional[float] = None
            locationZ: Optional[float] = None
            err2D: Optional[float] = None
            gpsTimestamp: Optional[int] = None
            positionEntry: Optional[PositionEntry] = self.locationHandler.getLastValidPosition(
            )
            if positionEntry is not None:
                locationX = positionEntry.longitude
                locationY = positionEntry.latitude
                locationZ = positionEntry.altitude
                err2D = positionEntry.err2D
                gpsTimestamp = positionEntry.timestamp

            # define angelbox properties
            timestamp: Final[int] = getUnixMilliTimestamp()
            errorCodes: list[int] = []
            if not self.bleScanner.isAlive:
                # report Bluetooth Failure error
                errorCodes.append(BluetoothFailureCode)
            if not self.locationHandler.isAlive:
                # report GPS Failure error
                errorCodes.append(GpsFailureCode)

            # make message and publish
            message: JSON = JSON({
                "version": self.version,
                "angelboxId": self.id,
                "locationX": locationX,
                "locationY": locationY,
                "locationZ": locationZ,
                "err2D": err2D,
                "gpsTimestamp": gpsTimestamp,
                "timestamp": timestamp,
                "errorCodes": errorCodes,
                "startupTimestamp": self.startupTimestamp,
            })
            publishMessage: str = json.dumps(message)

            print(publishMessage)

            # publish the heartbeat message
            result = self.networkHandler.publish(
                Networking.STATUS_TOPIC, publishMessage, 1, False)

            if result.rc == mqtt.MQTT_ERR_SUCCESS:
                print("Publish heartbeat: success")
            elif result.rc == mqtt.MQTT_ERR_NO_CONN:
                logging.error("Publish heartbeat: fail no connection")
            elif result.rc == mqtt.MQTT_ERR_QUEUE_SIZE:
                logging.error("Publish heartbeat: fail max queue")

            # sleep for 5 sec (for demo purpose)
            time.sleep(5)

        # cleanup
        self.stop()

    def stop(self) -> None:
        """
        This method sets the _shouldStop such that start() ends.
        """
        self._shouldStop = True


def main():
    """
    This method is entrypoint of the driver program.
    """
    # log level INFO or above
    logging.basicConfig(level=logging.INFO)

    # initialize the environment
    init(Command.UPDATE)

    # read the config
    config: Optional[JSON] = getConfig()
    if config is None:
        logging.critical("Failed to start AngelBox: missing config")
        return
    mqttConfig: Optional[JSON] = config.get("mqtt")
    if mqttConfig is None:
        logging.critical("Missing MQTT config")
        return
    broker_addr: Optional[str] = mqttConfig.get("broker_address")
    if broker_addr is None or type(broker_addr) is not str:
        logging.critical("Missing MQTT broker address")
        return
    broker_port: Optional[int] = mqttConfig.get("broker_port")
    if broker_port is None or type(broker_port) is not int:
        logging.critical("Missing MQTT broker port")
        return
    register_api_endpoint: Optional[str] = config.get("register_api_endpoint")
    if register_api_endpoint is None or type(register_api_endpoint) is not str or len(register_api_endpoint) <= 0:
        logging.critical("Missing Angelbox register Api endpoint")
        return

    # read the hardware MAC address
    mac_address: Optional[str] = getHardwareMacAddr()
    if mac_address is None or type(mac_address) is not str or len(mac_address) <= 0:
        logging.critical("Missing Hardware Mac address")
        return

    id: Optional[str] = None

    # get angelbox id or register id from server if not exists
    if checkAngelboxIdFileExist():
        # get id from file
        print("Get AngelBox id from file")
        id = getAngelboxId()
        if id is None or type(id) is not str or len(id) <= 0:
            logging.critical("Missing angelbox id from Id file")
            return
    else:
        # register id from server
        print("Register AngelBox id from server")
        id = registerAngelbox(register_api_endpoint, mac_address, __version__)
        if id is None or type(id) is not str or len(id) <= 0:
            logging.critical("Fail getting Angelbox id from server")
            return
        setAngelboxId(id)
        # verify id
        fileId: Optional[str] = getAngelboxId()
        if fileId is None or type(fileId) is not str or len(fileId) <= 0 or fileId != id:
            logging.critical("Registered Id fail verification")
            return
        print("Successfully registered Angelbox id")

    print(f"AngelBox ID: {id}")

    # create the AngelBox object
    angelBox: AngelBox = AngelBox(id, broker_addr, broker_port)

    try:
        # start the AngelBox object
        angelBox.start()
    except KeyboardInterrupt:
        # allow Ctrl+C can shutdown the program
        pass
    finally:
        angelBox.stop()
        logging.shutdown()
        sys.exit(0)


if __name__ == "__main__":
    main()
