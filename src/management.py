"""
This file contains the Management Module (functions).
"""

import json
import logging
import os
import shutil
import sys
from enum import IntEnum
from typing import ByteString, Final, Optional
from urllib.parse import urlparse
from urllib.request import urlcleanup, urlretrieve

from __init__ import __version__
from utils import (JSON, SYSTEMD_SERVICE_NAME, getLastCmd,
                   getUnixMilliTimestamp, print, restartSystemdService,
                   setLastCmd)


class Command(IntEnum):
    """
    This class is an Enum Class for holding command constants.
    """
    PING = 1
    REBOOT = 2
    UPDATE = 3


def handlePostCommandResponse() -> tuple[Optional[str], bool, Optional[str]]:
    """
    This function handles generating a command response after the program has finished executing a command.
    It returns a tuple of (command_id, is_success, command_response).
    """
    id: Optional[str] = None
    success: bool = False
    response: Optional[str] = None

    # read the last command executed
    lastCmd = getLastCmd()
    if lastCmd is None:
        return id, success, response

    id = lastCmd.get("command_id", "")
    if id == "":
        id = None
        setLastCmd(None)
        logging.warn("Invalid last command: missing command id")
        return id, success, response

    code: int = lastCmd.get("command", -1)
    if code == Command.REBOOT:
        # calculate duration of reboot
        start_time: int = lastCmd.get("timestamp", -1)
        current_time: int = getUnixMilliTimestamp()
        if start_time <= 0 or start_time >= current_time:
            response = '{"err": "reboot failed"}'
        else:
            success = True
            duration = (current_time - start_time) / 1e3
            response = '{"msg": "rebooted in ' + str(duration) + 's"}'
    elif code == Command.UPDATE:
        # report old version before updating
        version: str = lastCmd.get("version", "unknown version")
        success = True
        response = '{"msg": "updated to ' + \
            __version__ + ' from ' + version + '"}'
    else:
        # unknown command
        id = None
        setLastCmd(None)
        logging.warn("Invalid last command: unknown command")

    return id, success, response


def handleCommand(payload: ByteString) -> tuple[Optional[str], bool, Optional[str]]:
    """
    This function handles executing a command and optionally generating a command response.
    It returns a tuple of (command_id, is_success, command_response).
    """
    # converts the command payload into an object
    command: JSON = json.loads(bytes(payload))
    print("Command: {")
    for k, v in command.items():
        print(f"\t\"{k}\": {v}")
    print("}")

    id: str = command.get("id", "")
    if id == "":
        logging.error(f"Invalid command: {str(command)}")
        return None, False, '{"err": "missing command id"}'

    code: int = command.get("command", -1)
    success = False
    response = None
    if code == Command.PING:
        success, response = _handlePing(id)
    elif code == Command.REBOOT:
        success, response = _handleReboot(id)
    elif code == Command.UPDATE:
        try:
            # read the update URL supplied
            url: str = command.get("data", {}).get("update_url", "")
            # URL problems will lead to exception thrown
            success, response = _handleUpdate(id, url)
        except:
            logging.error(f"Missing update_url in command: {str(command)}")
            response = '{"err": "missing update url"}'
    else:
        logging.error(f"Unknown command: {str(command)}")
        response = '{"err": "unknown command"}'

    return id, success, response


def _handlePing(id: str) -> tuple[bool, str]:
    """
    This function handles the execution of the "ping" command.
    It responds with a simple "pong" message.
    """
    return True, '{"msg": "pong"}'


def _handleReboot(id: str) -> tuple[bool, Optional[str]]:
    """
    This function handles the execution of the "reboot" command.
    It executes the "/sbin/reboot" program (which is usually just running the systemctl command with necessary flags).
    """
    # record the command executed
    setLastCmd(JSON({
        "command_id": id,
        "command": Command.REBOOT,
        "timestamp": getUnixMilliTimestamp()
    }))

    # reboot
    try:
        os.system("/sbin/reboot")
    except:
        # prevent contradicting command response
        setLastCmd(None)

    # this line only reached if reboot failed
    return False, '{"err": "reboot did not execute"}'


def _handleUpdate(id: str, url: str) -> tuple[bool, Optional[str]]:
    """
    This function handles the execution of the "update" command.
    It validates the URL supplied and attempts to download the new binary.
    It replaces itself with the new binary and restarts the systmed service.
    """
    # record the command executed
    setLastCmd(JSON({
        "command_id": id,
        "command": Command.UPDATE,
        "version": __version__,
        "hasRestarted": False
    }))

    # much more work needed to support other protocols like ftp://
    allowed_schemas: Final[list[str]] = ["http", "https"]

    if url == "":
        return False, '{"err": "url cannot be empty"}'
    try:
        # analyze the URL
        parsedUrl = urlparse(url)
        if not parsedUrl.scheme in allowed_schemas:
            return False, '{"err": "url schema forbidden"}'

        parsedUrl.port  # throws ValueError if invalid

        # check the filename (assuming the filename is the last segment in the URL)
        segments = parsedUrl.path.strip("/").split("/")
        if len(segments) <= 0 or len(segments[-1]) <= 0:
            return False, '{"err": "url missing filename"}'
        filename = segments[-1]
        # make sure the filename matches the pattern "angelbox*.bin"
        if not (filename.startswith("angelbox") and filename.endswith(".bin")):
            return False, '{"err": "url filename not matching pattern \\"angelbox*.bin\\""}'
    except:
        return False, '{"err": "url format invalid"}'

    url = parsedUrl.geturl()
    # get the current executable path
    path: Final[str] = os.path.abspath(sys.argv[0])
    newPath: Final[str] = f"{path}.new"
    oldPath: Final[str] = f"{path}.old"
    try:
        # attempts to download the binary to the newPath
        urlretrieve(url, newPath)
    except Exception as e:
        logging.error(str(e))
        return False, '{"err": "failed downloading binary"}'

    try:
        # cleanup any temporary files created during the download
        urlcleanup()
    except:
        pass

    # removes the previous version
    if os.path.isfile(oldPath):
        os.remove(oldPath)

    # make the current version the "previous" version
    shutil.copyfile(path, oldPath, follow_symlinks=True)

    # replace new version as the "current" version
    os.rename(newPath, path)

    # make sure the binary is executable by any user
    os.chmod(path, 0o777)

    # restart the systemd service to make the change take effect
    restartSystemdService(SYSTEMD_SERVICE_NAME)

    sys.exit(0)  # gurrantee termination
