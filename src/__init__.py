"""
This file contains global variables.
"""

from typing import Final

# Global variable denoting program version
__version__: Final[str] = "1.6.1"
