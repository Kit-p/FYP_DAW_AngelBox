#!/bin/bash
# This script is intended to be run by sourcing only!
# i.e. `source ./setup.sh` or `. ./setup.sh`

clear
echo -e "\n==============Raspberry Pi Setup Script==============\n"
echo -e "\nStarting setup process ...\n"

# Sourcing check
(return 0 2>/dev/null) || {
    echo -e "\nPlease run this script by sourcing (source ./setup.sh)! Quitting ...\n"
    exit
}

# Quiet output
export DEBIAN_FRONTEND="noninteractive"

# Set home folder permission
sudo chmod 755 ~

# Create user bin directory and add it to PATH
mkdir -p ~/.local/bin
echo -e $'# set PATH so it includes user\'s private bin if it exists\nif [ -d "$HOME/bin" ] ; then\n    PATH="$HOME/bin:$PATH"\nfi\n\n# set PATH so it includes user\'s private bin if it exists\nif [ -d "$HOME/.local/bin" ] ; then\n    PATH="$HOME/.local/bin:$PATH"\nfi\n\n' >> ~/.bashrc
source ~/.bashrc

# Install ssl certificate for hkust network
echo -e "\nInstalling HKUST SSL certificate ...\n"
cd ~
sudo mkdir -p /usr/local/share/ca-certificates
curl -s -O https://cacerts.digicert.com/DigiCertGlobalRootCA.crt > /dev/null
sudo mv DigiCertGlobalRootCA.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates

# Use fast mirrors in /etc/apt/sources.list
echo -e "\nUsing fast mirrors in /etc/apt/sources.list ...\n"
sudo mv /etc/apt/sources.list /etc/apt/sources.list.bak
grep '^deb ' -m 1 /etc/apt/sources.list.bak | awk '
    BEGIN {
        print "# BEGIN CUSTOM MIRRORS";
        urls[0] = "http://free.nchc.org.tw/raspbian/raspbian";
        urls[1] = "http://mirror.ossplanet.net/raspbian/raspbian/";
    }
    {
        for (i in urls) {
            $2 = urls[i];
            $1 = "deb";
            print $0;
            $1 = "deb-src";
            print $0;
        }
    }
    END {
        print "# END CUSTOM MIRRORS";
        print "";
        print "# NOTE: Original sources.list renamed as sources.list.bak";
        print "#       To revert the changes, use the following command:";
        print "#       sudo mv -f /etc/apt/sources.list.bak /etc/apt/sources.list";
        print "";
    }
' > ~/sources.list
# sudo cat /etc/apt/sources.list.bak >> ~/sources.list (no need original content)
sudo mv ~/sources.list /etc/apt/sources.list

# Update and upgrade the system
echo -e "\nUpdating the packages and cleaning up ..."
echo -e "Note: This step takes a long time, please be patient\n"
sudo apt-get update > /dev/null
sudo apt-get dist-upgrade -qq
sudo apt-get autoremove --purge -qq
sudo apt-get autoclean

# ! TEMPORARY Update from Raspbian 10 (buster) to Raspbian 11 (bullseye)
echo -e "\nUpgrading from Raspbian 10 (buster) to Raspbian 11 (bullseye) ..."
echo -e "Note: Just answer 'Yes' or 'Y' when you are prompted during the upgrade"
echo -e "Also: This step takes a long time, please be patient\n"
sudo sed -i 's/buster/bullseye/g' /etc/apt/sources.list
sudo sed -i 's/buster/bullseye/g' /etc/apt/sources.list.d/*
sudo apt-get update > /dev/null
sudo apt-get upgrade --without-new-pkgs -qq
sudo apt-get dist-upgrade -qq
sudo apt-get autoremove --purge -qq
sudo apt-get autoclean

# Delete the current script
rm ./setup.sh

# Prepare for a reboot
echo -e "\nPreparing for a reboot to apply the changes ..."
echo -e "Note: The script will resume itself after you login\n"
# Add trigger to resume this script
echo -e "\nsource ~/setup.sh" >> ~/.bashrc

cat << EOF > ~/setup.sh
# Resume after reboot
clear
echo -e "\n==============Raspberry Pi Setup Script==============\n"
echo -e "\nResuming setup process from last reboot ...\n"

# Remove trigger for this script
sed -i /"^source ~\/setup.sh$"/d ~/.bashrc

# Install and set vim as the default editor
echo -e "\nInstalling and setting vim as default editor ...\n"
sudo apt-get install -y vim > /dev/null
echo -e "\n# Set default editor to vim\nexport EDITOR='vim'\nexport VISUAL='vim'\n" >> ~/.bashrc
source ~/.bashrc

# Install essential development tools
echo -e "\nInstalling essential development tools ...\n"
sudo apt-get install -y gcc gdb make libc6-dev git ssh > /dev/null

# Install GPS daemon and tools
echo -e "\nInstalling GPS daemon and tools ...\n"
sudo apt-get install -y gpsd gpsd-clients > /dev/null
sudo systemctl disable --now gpsd.socket
sudo systemctl enable --now gpsd.service

# Install requirements for VSCode server
echo -e "\nInstalling requirements for VSCode server ...\n"
sudo apt-get install -y libc6 libstdc++6 python2-minimal ca-certificates tar openssh-server bash curl wget > /dev/null

# Install python3 and pip3 and set python3 as default version of python
echo -e "\nInstalling Python development tools ...\n"
sudo apt-get install -y python3 python3-pip libglib2.0-dev > /dev/null
sudo apt-get install -y python-is-python3 > /dev/null

echo -e "\nInstalling Nuitka and autopep8 ...\n"
pip3 install --user -U -qq nuitka autopep8

echo -e "\nInstalling bluepy and paho-mqtt ...\n"
pip3 install --user -U -qq bluepy paho-mqtt

echo -e "\nInstalling Nuitka compilation tools ...\n"
sudo apt-get install -y patchelf ccache > /dev/null

# Clean up
echo -e "\nCleaning up orphan packages and unused files ...\n"
sudo apt-get autoremove --purge -qq
sudo apt-get autoclean > /dev/null

# Dialog output
export DEBIAN_FRONTEND="dialog"

# Generate ssh key
echo -e "\nGenerating ssh key for accessing GitLab ..."
echo -e "Note: You will be prompted to set a passphrase for the key."
echo -e "      Type a passphrase or simply press <Enter> for no passphrase.\n"
mkdir -p ~/.ssh
sudo chmod 700 ~/.ssh
ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519 -C "fyp2022@hkust"
sudo chmod 600 ~/.ssh/id*
sudo chmod 644 ~/.ssh/*.pub
eval `ssh-agent -s`
ssh-add ~/.ssh/id_ed25519
# sudo chmod 600 ~/.ssh/authorized_keys
# sudo chmod 600 ~/.ssh/config
echo -e "Successfully generated ssh key!\nThe public key is at '~/.ssh/id_ed25519.pub'."
echo -e "The file content will be displayed now.\nIMPORTANT: Copy it (exclude the BEGIN and END lines) to your GitLab Settings page."
echo -e "==========================BEGIN=========================="
cat ~/.ssh/id_ed25519.pub
echo -e "===========================END===========================\n"
read -s -n 1 -p "Press any key to continue ..."

echo -e "\nCloning the DAW_AngelBox repository under '~/FYP2122_GCH2' ...\n"
mkdir -p ~/FYP2122_GCH2
cd ~/FYP2122_GCH2
git clone git@gitlab.com:fyp2122_gch2/DAW_AngelBox.git
sudo chmod -R 755 ~/FYP2122_GCH2

# Delete the current script
rm ~/setup.sh

# raspi-config
echo -e "\nLaunching raspi-config ...\n"
sudo raspi-config
EOF

# Reboot to apply changes
sudo reboot
