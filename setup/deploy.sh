#!/bin/bash
# This script is intended to be run by sourcing only!
# i.e. `source ./deploy.sh` or `. ./deploy.sh`

clear
echo -e "\n==============AngelBox Deploy Script (Raspberry Pi)==============\n"
echo -e "\nStarting initial setup ...\n"

# Sourcing check
(return 0 2>/dev/null) || {
    echo -e "\nPlease run this script by sourcing (source ./deploy.sh)! Quitting ...\n"
    exit
}

# Quiet output
export DEBIAN_FRONTEND="noninteractive"

# Set home folder permission
sudo chmod 755 ~

# Create user bin directory and add it to PATH
mkdir -p ~/.local/bin
echo -e $'# set PATH so it includes user\'s private bin if it exists\nif [ -d "$HOME/bin" ] ; then\n    PATH="$HOME/bin:$PATH"\nfi\n\n# set PATH so it includes user\'s private bin if it exists\nif [ -d "$HOME/.local/bin" ] ; then\n    PATH="$HOME/.local/bin:$PATH"\nfi\n\n' >> ~/.bashrc
source ~/.bashrc

# Install ssl certificate for hkust network
echo -e "\nInstalling HKUST SSL certificate ...\n"
cd ~
sudo mkdir -p /usr/local/share/ca-certificates
curl -s -O https://cacerts.digicert.com/DigiCertGlobalRootCA.crt > /dev/null
sudo mv DigiCertGlobalRootCA.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates

# Use fast mirrors in /etc/apt/sources.list
echo -e "\nUsing fast mirrors in /etc/apt/sources.list ...\n"
sudo mv /etc/apt/sources.list /etc/apt/sources.list.bak
grep '^deb ' -m 1 /etc/apt/sources.list.bak | awk '
    BEGIN {
        print "# BEGIN CUSTOM MIRRORS";
        urls[0] = "http://free.nchc.org.tw/raspbian/raspbian";
        urls[1] = "http://mirror.ossplanet.net/raspbian/raspbian/";
    }
    {
        for (i in urls) {
            $2 = urls[i];
            $1 = "deb";
            print $0;
            $1 = "deb-src";
            print $0;
        }
    }
    END {
        print "# END CUSTOM MIRRORS";
        print "";
        print "# NOTE: Original sources.list renamed as sources.list.bak";
        print "#       To revert the changes, use the following command:";
        print "#       sudo mv -f /etc/apt/sources.list.bak /etc/apt/sources.list";
        print "";
    }
' > ~/sources.list
# sudo cat /etc/apt/sources.list.bak >> ~/sources.list (no need original content)
sudo mv ~/sources.list /etc/apt/sources.list

# Update and upgrade the system
echo -e "\nUpdating the packages and cleaning up ..."
echo -e "Note: This step takes a long time, please be patient\n"
sudo apt-get update > /dev/null
sudo apt-get dist-upgrade -qq
sudo apt-get autoremove --purge -qq
sudo apt-get autoclean

# ! TEMPORARY Update from Raspbian 10 (buster) to Raspbian 11 (bullseye)
echo -e "\nUpgrading from Raspbian 10 (buster) to Raspbian 11 (bullseye) ..."
echo -e "Note: Just answer 'Yes' or 'Y' when you are prompted during the upgrade"
echo -e "Also: This step takes a long time, please be patient\n"
sudo sed -i 's/buster/bullseye/g' /etc/apt/sources.list
sudo sed -i 's/buster/bullseye/g' /etc/apt/sources.list.d/*
sudo apt-get update > /dev/null
sudo apt-get upgrade --without-new-pkgs -qq
sudo apt-get dist-upgrade -qq
sudo apt-get autoremove --purge -qq
sudo apt-get autoclean

# Delete the current script
rm ./deploy.sh

# Prepare for a reboot
echo -e "\nPreparing for a reboot to apply the changes ..."
echo -e "Note: The script will resume itself after you login\n"
# Add trigger to resume this script
echo -e "\nsource ~/deploy.sh" >> ~/.bashrc

cat << EOF > ~/deploy.sh
# Resume after reboot
clear
echo -e "\n==============AngelBox Deploy Script (Raspberry Pi)==============\n"
echo -e "\nResumed from reboot, starting deployment setup ...\n"

# Remove trigger for this script
sed -i /"^source ~\/deploy.sh$"/d ~/.bashrc

# Install and set vim as the default editor
echo -e "\nInstalling and setting vim as default editor ...\n"
sudo apt-get install -y vim > /dev/null
echo -e "\n# Set default editor to vim\nexport EDITOR='vim'\nexport VISUAL='vim'\n" >> ~/.bashrc
source ~/.bashrc

# Install GPS daemon
echo -e "\nInstalling GPS daemon ...\n"
sudo apt-get install -y gpsd > /dev/null
sudo systemctl disable --now gpsd.socket
sudo systemctl enable --now gpsd.service

# Install common utilities
echo -e "\nInstalling common utilities ...\n"
sudo apt-get install -y ca-certificates tar openssh-server bash curl wget > /dev/null

# Clean up
echo -e "\nCleaning up orphan packages and unused files ...\n"
sudo apt-get autoremove --purge -qq
sudo apt-get autoclean > /dev/null

echo -e "\nDownloading AngelBox program ...\n"
curl -o angelbox.bin https://gitlab.com/api/v4/projects/35109637/packages/generic/angelbox/1.0.0/angelbox-linux-armv7l.bin
chmod 777 ./angelbox.bin

echo -e "\nExecuting AngelBox program ...\n"
sudo ./angelbox.bin
rm ./angelbox.bin

# Delete the current script
rm ~/deploy.sh

echo -e "\nCompleted AngelBox deployment! Please check 'AngelBox' > 'Pending' tab in the CMS web portal.\n"
EOF

# Reboot to apply changes
sudo reboot
