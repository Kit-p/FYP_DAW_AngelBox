# Dementia Anti-Wandering (FYP 2021-2022)
## Members
- PANG, Kit
- TAM, Tsz Chung
- WU, Tsz Yeung

---

## Description
This repository is the AngelBox driver program in the GCH2 Final Year Project.  

## Environment
- Python 3.9
- Debian-based Linux Operating System (with systemd)
- APT packages: bluez, libbluetooth3, gpsd, python3-gps, fuse, libfuse2, patchelf, ccache
- PIP modules: wheel, bluepy, gps>=3.22, paho-mqtt, requests, urllib3, Nuitka, zstandard, autopep8

## Folder Structure
| File / Folder         | Description                                                                   |
| ---:                  | :---                                                                          |
| `CONTRIBUTING.md`     | Instructions on how to setup a development environment                        |
| `Makefile`            | Offers the `make clean` command for clearing build outputs                    |
| `requirements.txt`    | Lists the pip packages needed, install with `pip install -r requirements.txt` |
| `.gitlab-ci.yml`      | Defines the GitLab CI pipeline                                                |
| `.gitignore`          | Excludes build outputs / cache from Git                                       |
| `.gitattribtues`      | Enforces line endings                                                         |
| `setup/*`             | Files needed to setup an AngelBox (do not delete)                             |
| `typings/*`           | To enforce strict typing for all PIP packages (do not delete)                 |
| `src/etc/*`           | Config files / MQTT certificates for the driver program (do not delete)       |
| `src/__init__.py`     | Defines global variables for the driver program, like program version         |
| `src/main.py`         | Defines the program entrypoint                                                |
| `src/bluetooth.py`    | Defines the Bluetooth Module, uses the `bluepy` package                       |
| `src/location.py`     | Defines the Location Module, uses the `gps` package from `python3-gps`        |
| `src/networking.py`   | Defines the Networking Module, uses the `paho-mqtt` package                   |
| `src/management.py`   | Defines the Management Module (functions), uses the `urllib3` package         |
| `src/utils.py`        | Defines utility functions to handle low level system interactions             |

---

## Deployment Setup (Raspberry Pi - Recommended)
> **IMPORTANT**: Deny all the system format warning during the setup.
1. Download the latest disk image `angelbox-raspios-bullseye-armv7l-lite.img` from the release.
2. Flash the micro SD card using the disk image above with [Raspberry Pi Imager](https://www.raspberrypi.org/software/).  
*Note: Available in choco with `choco install rpi-imager`.*  
**IMPORTANT: You must uncheck all the boxes in the Advanced Configurations/Options.**
3. If you want Wi-Fi, place a `wpa_supplicant.conf` in the `Boot` drive of the micro SD card.  
*Note: You can find an example file in the `setup/boot` directory in this repository. Modify it (using preferrably VSCode with **LF** line-ending or `notepad.exe`) to fill in the credentials before placing into the micro SD card. Read more [here](https://www.raspberrypi.org/documentation/configuration/wireless/headless.md).*
4. SSH and Wi-Fi should be enabled upon booting. Use `ping angelbox.local` to determine the ip address of the device.  
*Note: Please patiently wait for up to 5 minutes for initial boot if ping does not respond.*
5. `ssh -o UserKnownHostsFile=NUL pi@<ip-address>` and enter the secret password should get you a shell.  
*Note: Alternative command for ssh without ip address `ssh -o UserKnownHostsFile=NUL pi@angelbox`.*  
**IMPORTANT: The command for Linux or macOS is `ssh -o UserKnownHostsFile=/dev/null pi@<hostname/ip-address>`.**  
**IMPORTANT: If you encounter things like man-in-the-middle warning, run `clc ~/.ssh/known_hosts` on Windows PowerShell or `echo "" > ~/.ssh/known_hosts` on Linux or macOS terminal, then try again.**
6. The program has been registered as a systemd service upon first boot. Deployment completed.

## Deployment Setup (Raspberry Pi - Manual)
### Headless Mode
> **IMPORTANT**: Deny all the system format warning during the setup.
1. Flash the micro SD card with [Raspberry Pi Imager](https://www.raspberrypi.org/software/).  
*Note: Available in choco with `choco install rpi-imager`.*
2. Download all the files in the `/setup/boot` folder from this repository.
3. Place all the downloaded files in the `Boot` drive of the micro SD card.
4. Modify the `wpa_supplicant.conf` file to fill in the required credentials for connecting Wi-Fi. Read more [here](https://www.raspberrypi.org/documentation/configuration/wireless/headless.md).  
*Note: `notepad.exe` should be sufficient, but preferrably use VSCode and set the line-ending to **LF** if not set already.*
5. SSH and Wi-Fi should be enabled upon booting. Use `ping raspberrypi.local` to determine the ip address of the device.  
*Note: Please patiently wait for up to 5 minutes for initial boot if ping does not respond.*
6. `ssh -o UserKnownHostsFile=NUL pi@<ip-address>` and enter the default password `raspberry` should get you a shell.  
*Note: Alternative command for ssh without ip address `ssh -o UserKnownHostsFile=NUL pi@raspberrypi` or `ssh -o UserKnownHostsFile=NUL pi@<hostname>`.*  
**IMPORTANT: The command for Linux or macOS is `ssh -o UserKnownHostsFile=/dev/null pi@<hostname/ip-address>`.**  
**IMPORTANT: If you encounter things like man-in-the-middle warning, run `clc ~/.ssh/known_hosts` on Windows PowerShell or `echo "" > ~/.ssh/known_hosts` on Linux or macOS terminal, then try again.**
7. Run the setup script by `curl -O https://kit-p.github.io/FYP_DAW/setup/deploy.sh && source ./deploy.sh`.  
*Note: For security concern, you may inspect the script before executing.*
8. The program has been registered as a systemd service. Deployment completed.
### Headfull Mode (i.e. Direct Control)
> **IMPORTANT**: Follow guide above for Headless Mode except skipping steps 2-6.
