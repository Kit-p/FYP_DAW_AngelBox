SRCDIR		= ./src
BUILDDIR	= ./build
BINDIR		= ./bin

clean:
	@echo "Cleaning ..."
	rm -rf $(BUILDDIR) $(BINDIR)

.PHONY: clean
